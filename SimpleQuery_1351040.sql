Use HW01_ProjectManagement_1351040

--query 1
SELECT concat(e.FName,' ',e.Minit,'. ',e.LName),e.Address,e.Sex
FROM	EMPLOYEE as e

--query 2
SELECT FName
FROM	EMPLOYEE
WHERE DNO = 4

--query 3
SELECT BDate, Address
FROM	EMPLOYEE
WHERE concat(FName,' ',Minit,'. ',LName) = 'John B. Smith'

-- query 4
SELECT *
FROM EMPLOYEE
WHERE Address Like '%Houston, TX%'

-- query 5
SELECT *
FROM EMPLOYEE
WHERE BDate > '1-Jan-1950' AND BDate < '31-Dec-1959'

-- query 6
SELECT e.FName,e.Minit,e.LName,e.Salary+Salary*0.1
FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w
WHERE p.PName = 'ProductX'
		AND P.PNumber=W.PNO
		AND w.ESSN = e.SSN

-- query 7
SELECT *
FROM EMPLOYEE as e
WHERE e.Salary >= 30000
	AND e.Salary <=40000
	AND e.DNO = 5

--query 8
SELECT distinct e.*, p.PName, d.DName
FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w, DEPARTMENT as d
WHERE p.PNumber = w.PNO
	AND w.ESSN = e.SSN
	AND d.DNumber = e.DNO
ORDER BY d.DName ASC, e.FName ASC, e.LName ASC

--query 9
SELECT e.FName, e.LName, (SELECT YEAR(e.BDate)) as YearOfBirth
FROM EMPLOYEE as e

--query 10
SELECT d.DName, e.FName, e.LName, ( (SELECT YEAR(GETDATE()))-(SELECT YEAR(e.BDate)) ) as Age
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.SSN = d.MgrSSN

-- query 11
SELECT *
FROM EMPLOYEE
WHERE BDate > '1-Jan-1955' AND BDate < '31-Dec-1960'

-- query 12
SELECT e.FName, e.LName, e.Salary
FROM EMPLOYEE as e

-- query 13
SELECT distinct e.Salary
FROM EMPLOYEE as e

-- query 14
SELECT e.SSN, w.PNO
FROM EMPLOYEE as e, WORKS_ON as w
WHERE e.SSN = w.ESSN
	AND w.PNO in (1,2,3)

-- query 15
SELECT *
FROM EMPLOYEE as e
WHERE e.SupperSSN is null


--query 16
SELECT concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName, e.Address
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE d.DName = 'Research'
	AND d.DNumber = e.DNO

--query 17
SELECT emp.FName, emp.LName, sup.FName as SupFName, sup.LName as SupLName
FROM EMPLOYEE AS emp, EMPLOYEE AS sup
WHERE emp.SupperSSN=sup.SSN


--query 18
SELECT p.PNumber, p.DNum, emp.LName, emp.Address, emp.BDate
FROM EMPLOYEE as emp, PROJECT as p, DEPARTMENT as d
WHERE p.DNum=d.DNumber
	AND d.MgrSSN= emp.SSN
	AND p.PLocation='Stafford'

--query 19
SELECT distinct p.PNumber, concat(e.FName,' ',e.Minit,'. ',e.LName)
FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w, DEPARTMENT as d
WHERE
	(  d.MgrSSN = e.SSN
		AND
		concat(e.FName,' ',e.Minit,'. ',e.LName) Like '%Smith%'
	)
	OR
	(
		w.PNO = p.PNumber
		AND w.ESSN = e.SSN
		AND  concat(e.FName,' ',e.Minit,'. ',e.LName) Like '%Smith%'
	)


-- query 20
SELECT  concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName
FROM EMPLOYEE as e, DEPENDENT as d
WHERE e.SSN = d.ESSN
	AND e.Sex = d.Sex
	AND e.FName = d.Dependent_Name

-- query 21
SELECT d.DName,d.DNumber,d.MgrSSN,d.MgrStartDate
FROM DEPARTMENT as d JOIN EMPLOYEE as e ON d.DNumber = e.DNO
WHERE d.DNumber = e.DNO
GROUP BY d.DName,d.DNumber,d.MgrSSN,d.MgrStartDate
HAVING COUNT(*)>2




-- query 22
SELECT d.DName
FROM DEPARTMENT as d JOIN EMPLOYEE as e ON d.DNumber = e.DNO
WHERE d.DNumber = e.DNO
GROUP BY d.DName
HAVING COUNT(*)>3

-- query 23
SELECT e.FName,e.LName
FROM EMPLOYEE as e
WHERE NOT EXISTS(
	SELECT *
	FROM WORKS_ON as w
	WHERE w.ESSN = e.SSN
)

-- query 24
SELECT e.*
FROM EMPLOYEE as e
WHERE NOT EXISTS(
	SELECT *
	FROM DEPENDENT as d
	WHERE d.ESSN = e.SSN
)