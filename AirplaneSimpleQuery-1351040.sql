use Airplane_1351040
go

--query 1 Retrieve the id, full-name, address of the pilots who drove at least one plane typed �B747�
SELECT e.EmployeeID,e.FullName,e.Address
FROM EMPLOYEE as e, ALLOCATION as a, SCHEDULE as s
WHERE a.EID = e.EmployeeID
	AND a.FID = s.FID
	AND s.PType = 'B747'

-- query 2 Retrieve the id of the flights which had taken off at DCA airport between 14:00 and 18:00.
SELECT f.FlightID
FROM FLIGHT as f 
WHERE f.DepTime > '14:00'
	AND
	f.DepTime < '18:00'

--query 3 Retrieve the name of the employees assigned to the flights which had taken off at MIA airport on Nov 1, 2012
SELECT e.FullName
FROM EMPLOYEE as e, FLIGHT as f, SCHEDULE as s, ALLOCATION as a
WHERE f.Departure='MIA'
	AND s.Fdate = '11/01/2012'
	AND f.FlightID = s.FID
	AND a.FID = s.FID
	AND a.EID = e.EmployeeID

--query 4 Retrieve the code and manufacturer�s name of the air-planes used to take off at MIA airport.
SELECT p.PCode, pt.Manufacturer
FROM PLANE as p, PLANETYPE as pt, FLIGHT as f, SCHEDULE AS s
WHERE f.Departure = 'MIA'
	AND f.FlightID = s.FID
	AND s.PCode = p.PCode
	AND p.PType = pt.TypeID

--query 5 Retrieve the name and address of the customers reserved on flight 335 on Oct 31, 2012. The query result should be ordered in ascending of address.
SELECT c.FullName, c.Address
FROM CUSTOMER as c, RESERVATION as r
WHERE r.FID ='335'
	AND	r.Fdate ='10/31/2012'
	AND r.CID = c.CID
ORDER BY c.Address ASC
-- empty table

--query 6 Retrieve the name and home address of the pilots assigned to flight 335 on Oct 31, 2012. The result should be orderd in descending of the pilot�s name.
SELECT e.FullName, e.Address
FROM EMPLOYEE as e, SCHEDULE as s, ALLOCATION as a
WHERE a.FID = '335'
	AND a.Fdate = '10/31/2012'
	AND a.EID = e.EmployeeID
ORDER BY e.FullName DESC
--empty table

--query 7 Retrieve flight id, date for taking off, the name of the pilots assigned to the flights which landed at ORD airport
SELECT f.FlightID, s.Fdate, e.FullName
FROM FLIGHT as f, SCHEDULE as s, ALLOCATION as a, EMPLOYEE as e
WHERE f.Arrival ='ORD'
	AND f.FlightID = s.FID
	AND f.FlightID = a.FID
	AND a.EID = e.EmployeeID

--query 8 Retrieve the id, date for taking off of the flights which employee 1001 had been assign
SELECT distinct a.FID, s.Fdate
FROM SCHEDULE as s, ALLOCATION as a, EMPLOYEE as e
WHERE e.EmployeeID = '1001'
	AND e.EmployeeID = a.EID
	AND a.FID = s.FID

--query 9 Show the name and the address of all pilots who can drive some planes of �B747�.
SELECT e.FullName, e.Address
FROM EMPLOYEE as e, ABILITY as a
WHERE e.EmployeeID = a.EID
	AND a.PType = 'B747'

-- query 10 Retrieve the name and address of all employees whose salaries are from 200000 to 400000 and they can drive all planes of DC9
SELECT e.FullName, e.Address
FROM EMPLOYEE as e, ABILITY as a
WHERE e.Salary >= 200000
	AND e.Salary <= 400000
	AND a.PType = 'DC9'
	AND a.EID = e.EmployeeID

-- query 11 Show the information of all flights (flight ID, departure airport, arrival airport, departure time, arrival time) which Quang (pilot) had driven
SELECT f.*
FROM FLIGHT as f, ALLOCATION as a, EMPLOYEE as e
WHERE e.FullName = 'Quang'
	AND e.EmployeeID=a.EID
	AND a.FID = f.FlightID

--query 12 Show the information of all flights (flight ID, departure airport, and arrival airport) and the total time (minutes) of these flights. It�s assumed that all flights are taken off and landed at the same day
SELECT f.*, (DATEDIFF(MINUTE,f.DepTime,f.ArrTime)) as TotalTime
FROM FLIGHT as f

-- query 13 Show the name of all customers who have reserved in at least one flight which used Boeing manufacturer�s plane.
SELECT c.FullName
FROM CUSTOMER as c, RESERVATION as r, PLANETYPE as pt, PLANE as p, SCHEDULE as s
WHERE pt.Manufacturer='Boeing'
	AND pt.TypeID = p.PType
	AND p.PType = s.PType
	AND s.FID = r.FID
	AND r.CID = c.CID

--query 14 Show the name of all customers who have more than 2 reservations
SELECT c.FullName
FROM CUSTOMER as c
WHERE (SELECT COUNT(*) FROM RESERVATION as r WHERE r.CID = c.CID)>2

--query 15 Show the name of all employees who have been assigned to more than 2 scheduled flight.
SELECT c.FullName
FROM CUSTOMER as c
WHERE (SELECT COUNT(*)
		FROM RESERVATION as r, SCHEDULE as s
		 WHERE r.CID = c.CID
		 AND r.FID = s.FID
 )>2

 --query 16 Show the name of all employees who can drive the planes which are manufactured by Airbus.
 SELECT distinct e.FullName
 FROM EMPLOYEE as e, ABILITY as a,PLANETYPE AS pt
 WHERE a.EID = e.EmployeeID
	AND a.PType = pt.TypeID
	AND pt.Manufacturer = 'Airbus'

-- query 17 Show the Fdate, FID of all schedules which are no customer�s reservation.
SELECT s.Fdate, s.FID
FROM SCHEDULE as s
WHERE NOT EXISTS(SELECT *
				FROM CUSTOMER as c, RESERVATION as r
				WHERE c.CID = r.CID
					AND r.FID = s.FID
)

--query 18 Show the name, address, salary, etype of all employees who can�t drive any plane
SELECT e.FullName,e.Salary,e.EType
FROM EMPLOYEE as e, ABILITY as a
WHERE NOT EXISTS(SELECT *
				FROM ABILITY as a
				WHERE e.EmployeeID = a.EID
)

--query 19 Show the name, address and phone of all customers or employees who live at Nguyen Van Cu St.
SELECT c.FullName,c.Address,c.Phone
FROM CUSTOMER as c
WHERE c.Address Like '%Nguyen Van Cu%'
UNION ALL
SELECT e.FullName,e.Address,e.Phone
FROM EMPLOYEE as e
WHERE e.Address Like '%Nguyen Van Cu%'

--query 20 Show the pair of employees which have the same salary. Output format should be name of employee 1, name of employee 2, and the salary.
SELECT e1.FullName, e2.FullName, e1.Salary
FROM EMPLOYEE as e1, EMPLOYEE as e2
WHERE e1.Salary = e2.Salary
	AND e1.EmployeeID < e2.EmployeeID